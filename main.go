package main

import (
	"fmt"
	"sync"
	"net/http"

	"github.com/gorilla/websocket"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

var (
	upgrader = websocket.Upgrader{}
)

type Data struct {
	Text string `json:"text"`
}

var ReceiveData []Data
var connectionPool = struct {
	sync.RWMutex
	connections map[*websocket.Conn]struct{}
}{
	connections: make(map[*websocket.Conn]struct{}),
}


func WebsockHandler(c echo.Context) error {
	ws, err := upgrader.Upgrade(c.Response(), c.Request(), nil)
	if err != nil {
		return err
	}
	defer ws.Close()

	connectionPool.Lock()
	connectionPool.connections[ws] = struct{}{}
	defer func(connection *websocket.Conn) {
		connectionPool.Lock()
		delete(connectionPool.connections, connection)
		connectionPool.Unlock()
	}(ws)
	connectionPool.Unlock()

	// init
	err = ws.WriteJSON(ReceiveData)
	if err != nil {
		c.Logger().Error(err)
	}	

	for {
		// Write
		// err := ws.WriteMessage(websocket.TextMessage, []byte("Hello, Client!"))
		// if err != nil {
		// 	c.Logger().Error(err)
		// }

		// Read
		_, msg, err := ws.ReadMessage()
		if err != nil {
			c.Logger().Error(err)
		}
		fmt.Printf("%s\n", msg)
	}
}

func main() {
	ReceiveData = []Data{}
	ReceiveData = append(ReceiveData, Data{"aaa"})
	ReceiveData = append(ReceiveData, Data{"bbb"})
	
	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.File("/", "views/index.html")
	e.Static("/", "views/")
	e.GET("/ws", WebsockHandler)
	e.POST("/sendData", PostHandler)
	e.Logger.Fatal(e.Start(":1324"))
}

func PostHandler(c echo.Context) error {
    post := new(Data)
    if err := c.Bind(post); err != nil {
        return err
	}
	ReceiveData = append(ReceiveData, *post)
	sendMesseageToAllPool(*post)
    return c.JSON(http.StatusCreated, post)
}

func sendMesseageToAllPool(message Data) error {
	connectionPool.RLock()
	defer connectionPool.RUnlock()
	sendData := []Data{message}
	for connection := range connectionPool.connections {
		if err := websocket.WriteJSON(connection, sendData); err != nil{
			return err
		}
	}
	return nil
}